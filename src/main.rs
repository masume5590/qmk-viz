use anyhow::Context;
mod qmk;
use bevy::prelude::*;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_startup_system(setup)
        .run();
}

fn setup(mut commands: Commands, asset_server: Res<AssetServer>) {
    commands.spawn_bundle(Camera2dBundle::default());
    commands.spawn_bundle(SpriteBundle {
        texture: asset_server.load("key.png"),
        ..default()
    });
    let info =
        qmk::load_keyboard("splitkb/aurora/corne", "rev1").context("Failed to load keyboard info");
    println!("{:?}", info);
}
