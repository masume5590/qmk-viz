use std::{
    fs::File,
    io::{BufReader, Read},
    path::Path,
};

use anyhow::{Context, Result};
use json_comments::StripComments;
use serde_derive::Deserialize;
use serde_derive::Serialize;
use std::collections::HashMap;

const QMK_BASE_FOLDER: &str = "/Users/dbuschtoens/git/private/qmk_firmware";

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct KeyboardInfo {
    pub keyboard_name: String,
    pub layout_aliases: Option<HashMap<String, String>>,
    pub layouts: Option<HashMap<String, Layout>>,
    pub encoders: Option<Encoder>,
    pub rgb_matrix: Option<RgbMatrix>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct Layout {
    pub layout: Vec<LayoutKey>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct LayoutKey {
    pub label: String,
    pub matrix: [u8; 2],
    pub x: f32,
    pub y: f32,
}

#[derive(Default, Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct Encoder {
    pub enabled: bool,
}

#[derive(Default, Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct RgbMatrix {
    pub layout: Vec<LayoutRgb>,
}

#[derive(Default, Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct LayoutRgb {
    pub flags: u8,
    pub matrix: Option<[u8; 2]>,
    pub x: u8,
    pub y: u8,
}
pub fn load_keyboard(name: &str, revision: &str) -> Result<KeyboardInfo> {
    let path = Path::new(QMK_BASE_FOLDER)
        .join("keyboards")
        .join(name)
        .join(revision)
        .join("info.json");
    let file = File::open(path).context("Failed to open file")?;
    let mut reader = BufReader::new(file);
    let mut contents = String::new();
    reader
        .read_to_string(&mut contents)
        .context("Failed to read file")?;
    let contents = StripComments::new(contents.as_bytes());
    let info: KeyboardInfo = serde_json::from_reader(contents).context("Failed to parse JSON")?;
    Ok(info)
}
